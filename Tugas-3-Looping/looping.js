// No. 1 Looping While
var number = 2;
console.log("SOAL NO. 1");

console.log("LOOPING PERTAMA");
while(number <= 20){
	console.log(number + " - I love coding");
	number = number+2;
}

console.log("LOOPING KEDUA");
while(number > 2){
	number = number-2;
	console.log(number + " - I will become a mobile developer");
}
console.log("\n");

//====================================================================================
// No. 2 Looping menggunakan for
var jumlahMax = 20;
console.log("SOAL NO. 2");

for (var i =  1; i <= jumlahMax; i++) {
	if (i%2 == 0) {
		console.log(i + " - Berkualitas");
	} else if (i%3 == 0) {
		console.log(i + " - I Love Coding");
	} else {
		console.log(i + " - Santai");
	}
}
console.log("\n");


//====================================================================================
// No. 3 Membuat Persegi Panjang #
var jumlahTagarKolom = 8;
var jumlahTagarBaris = 4;
var tagar = '';
console.log("SOAL NO. 3");

for (var i = 0; i < jumlahTagarBaris; i++) {
	for (var j = 0; j < jumlahTagarKolom; j++) {
		tagar = tagar.concat('#');
	}
	console.log(tagar);
	tagar = '';
}
console.log("\n");

//====================================================================================
// No. 4 Membuat Tangga
var jumlahBaris = 7
var hasilTagar = '';
console.log("SOAL NO. 4");

for (var i = jumlahBaris; i > 0 ; i--) {
	for (var j=1; j <= jumlahBaris; j++) {
		if (j >= i ) {
			hasilTagar = hasilTagar.concat('#');
		}
	}
	console.log(hasilTagar);
	hasilTagar = '';
}

console.log("\n");


//====================================================================================
// No. 5 Membuat Papan Catur
var ukuranKolom = 8;
var ukuranBaris = 8;
var hasil = '';
var simbol1 = '#';
var simbol2 = ' ';
console.log("SOAL NO. 5");

for (var i = 1; i <= ukuranBaris; i++) {
	for (var j = 1; j <= ukuranKolom; j++) {
		if (j%2 == 0) {
			hasil = hasil.concat(simbol1);
		} else {
			hasil = hasil.concat(simbol2);
		}
	}
	console.log(hasil);
	hasil = '';

	if (i%2 == 0) {
		simbol1 = '#';
		simbol2 = ' ';
	} else {
		simbol1 = ' ';
		simbol2 = '#';
	}
}