import React, { useEffect } from 'react'
import { View, Text, StyleSheet, Image, TextInput, SafeAreaView, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons'
import { Data } from "./dummy";

export default function Skill() {
    useEffect(() => {
        console.log(Data)
    }, [])

    return(
        <>
        <View style={styles.backButton}>
            <TouchableOpacity onPress={() => console.log("Back")}>
                <FontAwesome5 name="arrow-left" size={32} color="black" />
            </TouchableOpacity>
        </View>
        <SafeAreaView style={styles.container}>
            <View style={styles.headerContent}>
                <Text style={[styles.title, {fontSize: 28}]}>Daftar Skill</Text>
                <View style={styles.lineStyle}/>
                <Text style={[styles.title, {fontSize: 24}]}>Bayu Arafli Fauzi</Text>
            </View>
            <View style={styles.bodyContent}>
                <SafeAreaView>
                    <FlatList
                        style={{marginBottom: 100}}
                        data={Data}
                        keyExtractor={(item) => item.id}
                        renderItem={({item}) => {
                            return(
                                <>
                                    <View style={styles.box}>
                                        <View style={styles.headerBox}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={{fontSize: 18, fontWeight: 'bold'}}>{item.nameSkill} | </Text>
                                                <Text style={{fontSize: 13, paddingTop: 3}}>{item.type}</Text>
                                            </View>
                                            <View style={styles.lineHorizontal}/>
                                            <View style={{flexDirection: 'row'}}>
                                                <View style={{width: 158, alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image
                                                        style={styles.logoBox}
                                                        source={item.image}
                                                    />
                                                </View>
                                                <View style={styles.lineVertical}/>
                                                <View style={{width: 162,alignItems: 'center', justifyContent: 'center'}}>
                                                <Text style={{fontSize: 18}}>{item.level}</Text> 
                                                <Text style={{fontSize: 48, fontWeight: 'bold'}}>{item.persen}</Text> 
                                                </View>
                                            </View>
                                        </View>
                                        <View style={styles.bodyBox}></View>
                                    </View>
                                </>
                            )
                        }}
                    />
                </SafeAreaView>
            </View>
        </SafeAreaView>
        </>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'

    },
    title:{
        fontFamily: 'Roboto'
    },
    lineHorizontal:{
        width: 315,
        height: 4, 
        backgroundColor: '#E5E5E5',
        marginTop: 5
    },
    lineVertical:{
        width: 4,
        height: 98, 
        backgroundColor: '#E5E5E5'
    },
    lineStyle:{
        width: 285,
        height: 4, 
        backgroundColor: '#035863',
        marginTop: 8
    },
    headerContent:{
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    bodyContent:{
        marginTop: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    backButton:{
        alignItems: 'flex-start',
        marginLeft: 20,
        marginTop: 50 
    },
    box:{
        backgroundColor: '#C4C4C4',
        width: 335,
        height: 141,
        borderRadius: 5,
        marginTop: 10
    },
    headerBox:{
        marginLeft: 10,
        marginTop: 10
    },
    logoBox:{
        height: 88,
        width: 88,
        borderRadius: 100
    },
})