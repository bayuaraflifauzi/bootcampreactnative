import React from 'react'
import { View, Text, StyleSheet, Image, TextInput, SafeAreaView, TouchableOpacity  } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons'

export default function About() {
    return(
        <>
        <View style={styles.offButton}>
            <TouchableOpacity onPress={() => console.log("Log Out")}>
                <FontAwesome5 name="power-off" size={32} color="black" />
            </TouchableOpacity>
        </View>
        <SafeAreaView style={styles.container}>
            <View style={styles.headerContent}>
                <Image
                    style={styles.logoStyle}
                    source={ require('./assets/LOGO.png') }
                />
                <Text style={styles.title}>Tentang Saya</Text>
                <View style={styles.lineStyle}/>
            </View>
            <View style={styles.bodyContent}>
                <Image
                    style={styles.photoProfile}
                    source={ require('./images/pp.png') }
                />
                <Text style={{marginBottom: 10, fontSize: 28, fontWeight: 'bold'}}>Bayu Arafli Fauzi</Text>
                <Text style={{marginBottom: 10, fontSize: 18}}>Software Engineering</Text>
            </View>
            <View style={styles.footerContent}>
                <View>
                    <TouchableOpacity
                        style={[styles.button, {backgroundColor: "#035863"}]}
                        onPress={() => console.log("skill")}
                    >
                        <Text style={styles.textButton}>Skill</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.button, {backgroundColor: "#FC6D26"}]}
                        onPress={() => console.log("Gitlab")}
                    >
                        <Text style={styles.textButton}>Gitlab</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.contact}>
                    <TouchableOpacity onPress={() => console.log("WhatsApp")}>
                        <Image
                            style={styles.photoContact}
                            source={ require('./assets/whatsapp.png') }
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => console.log("Link In")}>
                        <Image
                            style={[styles.photoContact,{marginLeft: 10, marginRight: 10}]}
                            source={ require('./assets/linkedin.png') }
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => console.log("G-Plus")}>
                        <Image
                            style={styles.photoContact}
                            source={ require('./assets/google-plus.png') }
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
        </>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'

    },
    logoStyle:{
        height: 62,
        width: 227
    },
    title:{
        marginTop: 38,
        fontSize: 24,
        fontFamily: 'Roboto'
    },
    lineStyle:{
        width: 285,
        height: 4, 
        backgroundColor: '#035863',
        marginTop: 8
    },
    input:{
        borderWidth: 1,
        backgroundColor: '#DFDBDB',
        borderColor: '#DFDBDB',
        borderRadius: 5,
        width: 285,
        height: 40,
        marginTop: 5,
        marginBottom: 10
    },
    button:{
        alignItems: "center",
        justifyContent: 'center',
        width: 285,
        height: 40,
        borderRadius: 8,
        marginBottom: 10
    },
    textButton:{
        color: 'white'
    },
    headerContent:{
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    bodyContent:{
        marginTop: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    footerContent:{
        marginTop: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    photoProfile:{
        width: 165,
        height: 165,
        borderRadius: 100,
        marginBottom: 10
    },
    contact:{
        flexDirection: 'row'
    },
    photoContact:{
        width: 65,
        height: 65,
        borderRadius: 5
    },
    offButton:{
        alignItems: 'flex-end',
        marginRight: 20,
        marginTop: 50 
    }
})