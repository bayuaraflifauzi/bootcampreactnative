import React from 'react'
import { StyleSheet, Text, Button, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { NavigationContainer } from '@react-navigation/native'

import Login from '../Pages/LoginScreen'
import About from '../Pages/AboutScreen'
import Register from '../Pages/RegisterScreen'
import Skill from '../Pages/SkillScreen'
import Certificate from '../Pages/CertificateScreen'
import Config from '../Pages/ConfigScreen'

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name='Login Screen' component={Login} />
                <Stack.Screen name='Register Screen' component={Register} />
                <Stack.Screen name='Skill Screen' component={MainApp} />
                <Stack.Screen name='About Screen' component={MyDrawwer} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp = () => (
    <Tab.Navigator>
        <Tab.Screen name="Skill" component={Skill} />
        <Tab.Screen name="Sertifikasi" component={Certificate} />
    </Tab.Navigator>
    
)

const MyDrawwer = () => (
    <Drawwer.Navigator>
        <Drawwer.Screen name="About" component={About} />
        <Drawwer.Screen name="Pengaturan" component={Config} />
    </Drawwer.Navigator>
)
