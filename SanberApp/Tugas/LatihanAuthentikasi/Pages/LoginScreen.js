import React, { useEffect, useState } from 'react'
import { Alert, View, Text, StyleSheet, Image, TextInput, SafeAreaView, TouchableOpacity  } from 'react-native'
import * as firebase from 'firebase';

export default function Login({navigation}) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const firebaseConfig = {
        apiKey: "AIzaSyAxDPF28-a9zZ0v_4rXhz8mDCqZKlYYRTA",
        authDomain: "sanberapp-bfada.firebaseapp.com",
        projectId: "sanberapp-bfada",
        storageBucket: "sanberapp-bfada.appspot.com",
        messagingSenderId: "193517219018",
        appId: "1:193517219018:web:0fe96b57ba45c4c9e09833",
        measurementId: "G-HBNT5XXZJT"
    };
    // Initialize Firebase
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);   
    }

    const submit = () =>{
        const data = {
            email, password
        }
        console.log(data)

        if (data.email == '' || data.password == '') {
            Alert.alert("Error","Harap form terisi semua!")
            setPassword('')
            setEmail('')
        } else {
            firebase.auth().signInWithEmailAndPassword(email, password)
            .then((res)=>{
                navigation.replace("About Screen", {
                    screen: 'About', params:{
                        screen: 'Keluar'
                    }
                })
            }).catch((err)=>{
                Alert.alert("Error","Login gagal! silahkan lakukan kembali")
            })
        }
    }

    return(
        <SafeAreaView style={styles.container}>
            <View style={styles.headerContent}>
                <Image
                    style={styles.logoStyle}
                    source={ require('./assets/LOGO.png') }
                />
                <Text style={styles.title}>Sign In</Text>
                <View style={styles.lineStyle}/>
            </View>
            <View style={styles.bodyContent}>
            <Text>Email</Text>
                <TextInput 
                    style={styles.input}
                    placeholder= "Masukkan email"
                    value= {email}
                    onChangeText= {(value)=>setEmail(value)}
                />

                <Text>Password</Text>
                <TextInput 
                    style={styles.input} 
                    placeholder= "Masukkan password"
                    value= {password}
                    onChangeText= {(value)=>setPassword(value)}
                />
            </View>
            <View style={styles.footerContent}>
                <TouchableOpacity
                    style={[styles.button, {backgroundColor: "#035863"}]}
                    // onPress={() => navigation.navigate("About Screen", {
                    //     screen: 'About', params:{
                    //         screen: 'Keluar'
                    //     }
                    // })}
                    onPress={submit}
                >
                    <Text style={styles.textButton}>Login</Text>
                </TouchableOpacity>
                <Text style={{marginBottom: 10}}>atau</Text>
                <TouchableOpacity
                    style={[styles.button, {backgroundColor: "#BBB4B4"}]}
                    onPress={() => navigation.navigate("Register Screen")}
                >
                    <Text style={styles.textButton}>Register</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        paddingTop: 20

    },
    logoStyle:{
        height: 62,
        width: 227
    },
    title:{
        marginTop: 38,
        fontSize: 24,
        fontFamily: 'Roboto'
    },
    lineStyle:{
        width: 285,
        height: 4, 
        backgroundColor: '#035863',
        marginTop: 8
    },
    input:{
        borderWidth: 1,
        backgroundColor: '#DFDBDB',
        borderColor: '#DFDBDB',
        borderRadius: 5,
        width: 285,
        height: 40,
        marginTop: 5,
        marginBottom: 10,
        paddingHorizontal: 10
    },
    button:{
        alignItems: "center",
        justifyContent: 'center',
        width: 285,
        height: 40,
        borderRadius: 8,
        marginBottom: 10
    },
    textButton:{
        color: 'white'
    },
    headerContent:{
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    bodyContent:{
        marginTop: 25,
        backgroundColor: 'white'
    },
    footerContent:{
        marginTop: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    }
})