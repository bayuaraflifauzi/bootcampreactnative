const Data = [
    {
        id : "1",
        image: require("./images/Logo.png"),
        nameSkill:'Javascript' ,
        type:'Bahasa Peograman',
        level:'Basic',
        persen:'85%'
    },
    {
        id : "2",
        image: require("./images/Logo.png"),
        nameSkill:'Laravel' ,
        type:'Framework',
        level:'Advance',
        persen:'75%'
    },
    {
        id : "3",
        image: require("./images/Logo.png"),
        nameSkill:'Adobe XD' ,
        type:'Tools',
        level:'Basic',
        persen:'65%'
    },
    {
        id : "4",
        image: require("./images/Logo.png"),
        nameSkill:'Java' ,
        type:'Bahasa Pemograman',
        level:'Basic',
        persen:'60%'
    },
    {
        id : "5",
        image: require("./images/Logo.png"),
        nameSkill:'Figma' ,
        type:'Tools',
        level:'Basic',
        persen:'85%'
    }
]

export { Data }