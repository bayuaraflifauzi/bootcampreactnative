import React from 'react'
import { View, Text, StyleSheet, Image, TextInput, SafeAreaView, TouchableOpacity  } from 'react-native'
import { Ionicons } from '@expo/vector-icons';

export default function Register({navigation}) {
    return(
        <SafeAreaView style={styles.container}>
            <View style={styles.headerContent}>
                <Image
                    style={styles.logoStyle}
                    source={ require('./assets/LOGO.png') }
                />
                <Text style={styles.title}>Register</Text>
                <View style={styles.lineStyle}/>
            </View>
            <View style={styles.bodyContent}>
                <Text>Username</Text>
                <TextInput style={styles.input} />

                <Text>Email</Text>
                <TextInput style={styles.input} />

                <Text>Password</Text>
                <TextInput style={styles.input} />

                <Text>Konfirmasi Password</Text>
                <TextInput style={styles.input} />
            </View>
            <View style={styles.footerContent}>
                <TouchableOpacity
                    style={[styles.button, {backgroundColor: "#035863"}]}
                    onPress={() => navigation.navigate("About Screen", {
                        screen: 'About', params:{
                            screen: 'Keluar'
                        }
                    })}
                >
                    <Text style={styles.textButton}>Daftar</Text>
                </TouchableOpacity>
                <Text style={{marginBottom: 10}}>atau</Text>
                <TouchableOpacity
                    style={[styles.button, {backgroundColor: "#BBB4B4"}]}
                    onPress={() => navigation.navigate("Login Screen")}
                >
                    <Text style={styles.textButton}>Masuk</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        paddingTop: 20

    },
    logoStyle:{
        height: 62,
        width: 227
    },
    title:{
        marginTop: 38,
        fontSize: 24,
        fontFamily: 'Roboto'
    },
    lineStyle:{
        width: 285,
        height: 4, 
        backgroundColor: '#035863',
        marginTop: 8
    },
    input:{
        borderWidth: 1,
        backgroundColor: '#DFDBDB',
        borderColor: '#DFDBDB',
        borderRadius: 5,
        width: 285,
        height: 40,
        marginTop: 5,
        marginBottom: 10
    },
    button:{
        alignItems: "center",
        justifyContent: 'center',
        width: 285,
        height: 40,
        borderRadius: 8,
        marginBottom: 10
    },
    textButton:{
        color: 'white'
    },
    headerContent:{
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    bodyContent:{
        marginTop: 25,
        backgroundColor: 'white'
    },
    footerContent:{
        marginTop: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    }
})