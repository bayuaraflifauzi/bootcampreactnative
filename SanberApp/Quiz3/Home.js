import React, { useEffect } from 'react'
import { useState } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, SafeAreaView } from 'react-native'
import { Data }from './data'
export default function Home({route, navigation}) {
    const { username } = route.params;
    const [totalPrice, setTotalPrice] = useState(0);

    var currencyFormat=(num)=> {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
      };

    const updateHarga =(price)=>{
        console.log("UpdatPrice : " + price);
        const temp = Number(price) + totalPrice;
        console.log(temp)
        setTotalPrice(temp)
        
        //? #Bonus (10 poin) -- HomeScreen.js --
        //? agar harga dapat update misal di tambah lebih dari 1 item atau lebih -->
            
    }
    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            <View style={{alignItems:'center'}}>
            {//? #Soal No 2 (15 poin) -- Home.js -- Function Home
            //? Buatlah 1 komponen FlatList dengan input berasal dari data.js   
            //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal) -->
            
            //? #Soal No 3 (15 poin) -- HomeScreen.js --
             //? Buatlah styling komponen Flatlist, agar dapat tampil dengan baik di device untuk layouting bebas  --> */             
            }
            <SafeAreaView>
                <FlatList
                    numColumns={2}
                    style={{marginBottom: 100}}
                    data={Data}
                    keyExtractor={(item) => item.id}
                    renderItem={({item}) => {
                        return(
                            <>
                                <View style={styles.box}>
                                    <View style={styles.headerBox}>
                                        <View style={{alignItems: 'center'}}>
                                            <Text style={{fontSize: 18, fontWeight: 'bold'}}>{item.title}</Text>
                                            <Text style={{fontSize: 13, paddingTop: 3}}>({item.type})</Text>
                                            <Text style={{fontSize: 15, paddingTop: 3}}>Rp {item.harga}</Text>
                                        </View>
                                        <View style={styles.lineHorizontal}/>
                                    </View>
                                    <View style={styles.bodyBox}></View>
                                    <View style={{alignItems: 'center', justifyContent: 'center', paddingVertical: 13}}>
                                        <Image
                                            style={styles.logoBox}
                                            source={item.image}
                                        />
                                    </View>
                                    <View style={{flexDirection:'row', paddingHorizontal: 10, paddingBottom: 10}}> 
                                        <Text style={{fontSize: 12, flex: 1, flexWrap: 'wrap'}}>Desc : {item.desc}</Text>
                                    </View>
                                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                        <TouchableOpacity
                                            style={[styles.button, {backgroundColor: "#0E86D4"}]}
                                            onPress={() => updateHarga(item.harga)}
                                        >
                                            <Text style={styles.textButton}>Beli</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </>
                        )
                    }}
                />
            </SafeAreaView>

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white', 
    },  
    content:{
        width: 150,
        height: 220,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',    
    },
    lineHorizontal:{
        width: 145,
        height: 4, 
        backgroundColor: '#E5E5E5',
        marginTop: 5
    },
    box:{
        backgroundColor: '#C4C4C4',
        alignSelf: 'flex-start',
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 5,
        marginTop: 10,
        marginRight: 5
    },
    headerBox:{
        marginTop: 10,
        paddingHorizontal: 10
    },
    logoBox:{
        height: 100,
        width: 100
    },
    button:{
        alignItems: "center",
        justifyContent: 'center',
        width: 100,
        height: 40,
        borderRadius: 8,
        marginBottom: 10
    },
    textButton:{
        color: 'white'
    }
        
})
