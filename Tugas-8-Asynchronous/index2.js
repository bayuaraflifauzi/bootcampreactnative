let readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
let readingTime = 4000;

var read = (time, books, index=0) => {
	if (index < books.length) {
		readBooksPromise(time, books[index])
		.then((fulfilled) => {
			if (fulfilled > 0) {
				index = index + 1;
				read(fulfilled, books, index)
			}
		})
		.catch((error) => {
			return error;
		});
	}
}

// driver
read(readingTime, books);