// di index.js
let readBooks = require('./callback.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Tulis code untuk memanggil function readBooks di sini
let readingTime = 9000;

read = (time, books, index=0) => {
	if (index < books.length) {
		readBooks(time, books[index], (timeLeft) => {
			if (timeLeft < time ) {
				index = index + 1;
				read(timeLeft, books, index)
			}
		})
	} else {
		console.log("\nBuku tidak ada lagi, bacaan selesai");
	}
}

// driver
read(readingTime, books);