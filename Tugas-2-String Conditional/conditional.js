// Soal If-Else
var nama = "Bayu";
var peran = "Werewolf";
var notif;

if(nama.length == 0){
    notif = "Nama harus diisi!";
}

if (peran.length > 0) {
    if (peran.toLowerCase() == "penyihir") {
        notif = "Selamat datang di Dunia Werewolf, " + nama + "\nHalo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!";
    } else if (peran.toLowerCase() == "guard") {
        notif = "Selamat datang di Dunia Werewolf, " + nama + "\nHalo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.";
    } else if (peran.toLowerCase() == "werewolf") {
        notif = "Selamat datang di Dunia Werewolf, " + nama + "\nHalo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!";
    } else {
        notif = "Halo " + nama + ", Silakan pilih peranmu antara Penyihir, Guard, dan Werewolf!";
    }
} else {
    notif = "Halo " + nama + ", Pilih peranmu untuk memulai game!";
}

console.log("SOAL IF-ELSE");
console.log(notif);
console.log("\n");

//======================================================================================================
// Soal Switch Case
var hari = 31; 
var bulan = 12; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 

console.log("SOAL SWITCH CASE");
if (hari <= 0 || hari > 31) {
    console.log("Maaf, tanggal yang diinput hanya dari 1 - 31");
} else if (bulan <= 0 || bulan > 12) {
    console.log("Maaf, bulan yang diinput hanya dari 1 - 12");
} else if (tahun < 1900 || hari > 2200) {
    console.log("Maaf, tahun yang diinput hanya dari 1900 - 2200");
} else {
    switch(bulan) {
        case 1: bulan = "Januari"; break;
        case 2: bulan = "Februari"; break;
        case 3: bulan = "Maret"; break;
        case 4: bulan = "April"; break;
        case 5: bulan = "Mei"; break;
        case 6: bulan = "Juni"; break;
        case 7: bulan = "Juli"; break;
        case 8: bulan = "Agustus"; break;
        case 9: bulan = "September"; break;
        case 10: bulan = "Oktober"; break;
        case 11: bulan = "November"; break;
        case 12: bulan = "Desember"; break;
        default: console.log("Maaf, bulan yang diinput hanya dari 1 - 12") ; break;
    }
    
    console.log(hari + " " + bulan + " " + tahun);
}