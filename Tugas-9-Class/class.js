// 1. Animal Class
console.log("1. Animal Class")
// Release 0
class Animal {
    // Code class di sini
    constructor(name, legs = 4, cold_blooded = false) {
        this.name = name
        this.legs = legs
        this.cold_blooded = cold_blooded
    }

    getName(){
    	return this.name;
    }

    setName(name){
    	this.name = name;
    }

    getLegs(){
    	return this.legs;
    }

    setLegs(legs){
    	this.legs = legs;
    }

    getColdBlooded(){
    	return this.cold_blooded;
    }

    setColdBlooded = (coldBlooded) => {
    	this.cold_blooded = coldBlooded;
    }
}
 
var sheep = new Animal("shaun");
 
console.log("Release 0")
console.log(sheep.getName()) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log("\n")

// Release 1
// Code class Ape dan class Frog di sini
class Ape extends Animal {
	constructor(name) {
		super(name);
	}

	yell(){
		return console.log("Auooo");
	}
}

class Frog extends Animal {
	constructor(name) {
		super(name);
	}

	jump(){
		return console.log("hop hop");
	}
}
 
console.log("Release 1")

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log("\n");

// ==============================================================================================================
// 2. Function to Class
console.log("2. Function to Class");

// class
class Clock {
	constructor({ template }) {
		this.template = template;
	}

	render(){
		var date = new Date();

	    var hours = date.getHours();
	    if (hours < 10) hours = '0' + hours;

	    var mins = date.getMinutes();
	    if (mins < 10) mins = '0' + mins;

	    var secs = date.getSeconds();
	    if (secs < 10) secs = '0' + secs;

	    var output = this.template
	      .replace('h', hours)
	      .replace('m', mins)
	      .replace('s', secs);

    	console.log(output);
    }

    stop(){
    	clearInterval(this.timer);
    };

    start(){
    	this.render();
    	this.timer = setInterval(() => this.render(), 1000);
  	};
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 