// 1. Mengubah fungsi menjadi fungsi arrow
console.log('SOAL NO 1');
/*
	==>ES5
	const golden = function goldenFunction(){
	  console.log("this is golden!!")
	}
	 
*/

// ES6
var golden = () => {
	console.log("this is golden!!");
}

golden();
console.log('\n\n')

// ==================================================================================================
// 2. Sederhanakan menjadi Object literal di ES6
console.log('SOAL NO 2');
/*
	//==> ES5
	const newFunction = function literal(firstName, lastName){
	  return {
	    firstName: firstName,
	    lastName: lastName,
	    fullName: function(){
	      console.log(firstName + " " + lastName)
	      return 
	    }
	  }
	}
*/

// ES6
var newFunction = (firstName, lastName) => {
	return {
		firstName,
		lastName,
        fullName() {
            return console.log(firstName + " " + lastName)
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName();
console.log('\n\n')

// ==================================================================================================
// 3. Destructuring
console.log('SOAL NO 3');
const newObject = {
	firstName: "Harry",
	lastName: "Potter Holt",
	destination: "Hogwarts React Conf",
	occupation: "Deve-wizard Avocado",
	spell: "Vimulus Renderus!!!"
}

/*
	//==> ES5
	const firstName = newObject.firstName;
	const lastName = newObject.lastName;
	const destination = newObject.destination;
	const occupation = newObject.occupation;
*/


// ES6
var { firstName, lastName, destination, occupation, spell } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)
console.log('\n\n')

// ==================================================================================================
// 4. Array Spreading
console.log('SOAL NO 4');
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

// ES5
// const combined = west.concat(east);

// ES6
const combined = [...west, ...east];

//Driver Code
console.log(combined)
console.log('\n\n')

// ==================================================================================================
// 5. Template Literals
console.log('SOAL NO 5');
const planet = "earth"
const view = "glass"

// ES5
// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam';

// ES6
var before = `Lorem ${view} dolor sit amet, 
consectetur adipiscing elit, ${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam`;
 
// Driver Code
console.log(before)