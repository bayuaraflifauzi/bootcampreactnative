// SOAL NO 1
console.log("SOAL NO 1");
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr) {
    
    if (arr.length == 0) {
        return console.log("Masukkan Array");
    }

    var obj = {};
    
    for (var i = 0; i < arr.length; i++) {

        var objBio = {};
        objBio["firstName"] = arr[i][0];
        objBio["lastName"] = arr[i][1];
        objBio["gender"] = arr[i][2];

        if (!arr[i][3]) {
            objBio["age"] = "Invalid Birth Year";
        } else if (arr[i][3]>thisYear) {    
            objBio["age"] = "Invalid Birth Year";
        } else {
            objBio["age"] = thisYear - arr[i][3];
        }


        obj[(i+1) + ". " + arr[i][0] + " " + arr[i][1]] = objBio;
    }
    console.log(obj)
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log("\n\n");

// ================================================================================================================
// SOAL NO 2
console.log("SOAL NO 2");
function shoppingTime(memberId, money) {
  
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }

    if (money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    }

    var bill = {};
    var listProduct = [];
    var product = [
        {
            id : 1,
            name : "Sepatu brand Stacattu",
            price : 1500000
        },
        {
            id : 2,
            name : "Baju brand Zoro",
            price : 500000
        },
        {
            id : 3,
            name : "Baju brand H&N",
            price : 250000
        },
        {
            id : 4,
            name : "Sweater brand Uniklooh",
            price : 175000
        },
        {
            id : 5,
            name : "Casing Handphone",
            price : 50000
        }
    ];

    bill.memberId = memberId;
    bill.money = money;

    for (var i = 0; i < product.length; i++) {
        if (money >= product[i].price) {
            if (!listProduct.includes(product[i].name)) {
                money = money - product[i].price;
                listProduct.push(product[i].name);
            }
        }
    }

    bill.listPurchased = listProduct;
    bill.changeMoney = money;

    return bill;

}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }

console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log("\n\n");

// ================================================================================================================
// SOAL NO 3
console.log("SOAL NO 3");
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    
    if (arrPenumpang.length == 0) {
        return "Masukkan Array";
    }

    var list = [];
    for (var i = 0; i < arrPenumpang.length; i++) {
        var obj = {};
        obj["penumpang"] = arrPenumpang[i][0];
        obj["naikDari"] = arrPenumpang[i][1];
        obj["tujuan"] = arrPenumpang[i][2];

        if (rute.indexOf(arrPenumpang[i][2]) > rute.indexOf(arrPenumpang[i][1])) {
            obj["bayar"] = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * 2000;
        } else {
            obj["bayar"] = (rute.indexOf(arrPenumpang[i][1]) - rute.indexOf(arrPenumpang[i][2])) * 2000;            
        }

        list.push(obj);
    }
    
    return list;
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]