// Soal No. 1 (Range) 
console.log("SOAL NO 1");
function range(startNum, finishNum){
	var number = [];
	if (startNum == null || finishNum == null) {
		return '-1';
	} else {
		if (startNum <= finishNum) {
			for (var i = startNum; i <= finishNum; i++) {
				number.push(i);
			}
		} else {
			for (var i = startNum; i >= finishNum; i--) {
				number.push(i);
			}
		}
		return number;
	} 

}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("\n\n");

// ===============================================================================================================
// Soal No. 2 (Range with Step)
console.log("SOAL NO 2");
function rangeWithStep(startNum, finishNum, step){
	var number = [];
	if (startNum == null || finishNum == null || step == null) {
		return '-1';
	} else {
		if (startNum <= finishNum) {
			while(startNum<=finishNum){
				number.push(startNum);
				startNum = startNum + step;
			}

		} else {
			while(startNum>=finishNum){
				number.push(startNum);
				startNum = startNum - step;
			}
		}
		return number;
	} 

}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log("\n\n");

// ===============================================================================================================
// Soal No. 3 (Sum of Range)
console.log("SOAL NO 3");
function sum(startNum, finishNum, step){
	var number = [];

	if (step == null) {
		step = 1;
	}

	if (finishNum == null) {
		return (startNum == null) ? startNum=0 : startNum;
	} else {
		if (startNum <= finishNum) {
			while(startNum<=finishNum){
				number.push(startNum);
				startNum = startNum + step;
			}

		} else {
			while(startNum>=finishNum){
				number.push(startNum);
				startNum = startNum - step;
			}
		}
		return number.reduce((a, b) => a + b);
	} 

}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("\n\n");

// ===============================================================================================================
// Soal No. 4 (Array Multidimensi)
console.log("SOAL NO 4");
function dataHandling(array){
	for (var i = 0; i < array.length; i++) {
		console.log("Nomor ID: " + array[i][0] + "\nNama Lengkap: " + array[i][1] + "\nTTL: " + array[i][2] + ", " + array[i][3] +  "\nHobi: " + array[i][4]);
		console.log("\n");
	}
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 

dataHandling(input);
console.log("\n\n");

// ===============================================================================================================
// Soal No. 5 (Balik Kata)
console.log("SOAL NO 5");
function balikKata(kata){
	var newWord = [];

	for (var i = (kata.length-1); i >= 0; i--) {
		newWord.push(kata[i]);
	}

	return String(newWord).replace(/,/gi,'');
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I
console.log("\n\n");

// ===============================================================================================================
// Soal No. 6 (Metode Array)
console.log("SOAL NO 6");
function dataHandling2(array){
	var arrayBaru = array.slice(0,4);
	arrayBaru.push("Pria");
	arrayBaru.push("SMA Internasional Metro");
	arrayBaru[1] = "Roman Alamsyah Elsharawy";
	arrayBaru[2] = "Provinsi Bandar Lampung";
	console.log(arrayBaru);

	var bulan;
	var date = arrayBaru[3];
	var currentDate = date.split("/");

	switch(parseInt(String(currentDate[1]).replace(/^0+/, ''))) {
        case 1: bulan = "Januari"; break;
        case 2: bulan = "Februari"; break;
        case 3: bulan = "Maret"; break;
        case 4: bulan = "April"; break;
        case 5: bulan = "Mei"; break;
        case 6: bulan = "Juni"; break;
        case 7: bulan = "Juli"; break;
        case 8: bulan = "Agustus"; break;
        case 9: bulan = "September"; break;
        case 10: bulan = "Oktober"; break;
        case 11: bulan = "November"; break;
        case 12: bulan = "Desember"; break;
        default: bulan = "Maaf, bulan yang diinput hanya dari 1 - 12" ; break;
    }

	console.log(bulan);

	var newDate = [];
	newDate.push(currentDate[2]);
	newDate.push(currentDate[0]);
	newDate.push(currentDate[1]);

	console.log(newDate);
	console.log(currentDate.join("-"));
	console.log(array[1]);
}

// Input
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);